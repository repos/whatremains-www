Title: Credits + Colophon 
Date: 2016-05-07

The team: [Amber Griffiths](http://www.ambergriffiths.info), [Dave Griffiths](http://www.pawfal.org/dave/blog/), [Arnaud Guillon](http://sumipto.com), [Aymeric Mansoux](https://bleu255.com/~aymeric) and [Marloes de Valk](http://pi.kuri.mu).

Curator/fellow New Archive Interpretations for Het Nieuwe Instituut: [Annet Dekker](http://aaaan.net)

Twitter: [@remwhat](https://twitter.com/remwhat)

E-mail: remwhat at bleu255 dot com

-----------------

This site is served by [nginx](https://nginx.org) on a [Debian](https://www.debian.org) operating system. The pages are generated as static HTML, CSS and image files using the [Pelican](http://blog.getpelican.com) [Python](https://www.python.org) framework. We contribute content on local [Git](https://git-scm.com) repositories and sync with each other via [GitLab](https://gitlab.com/plutoniancorp/whatremains-blog).

------------------

What remains is made possible with support from [Creative Industries Fund NL](http://www.stimuleringsfonds.nl) and [Het Nieuwe Instituut](http://hetnieuweinstituut.nl). Our server is kindly hosted by [Servus](http://servus.at).

<img class="noblock" src="../images/logo-SCI.jpg" alt="logo SCI"> 
<img class="noblock" src="../images/logo-HNI.jpg" alt="logo HNI">
<img class="noblock" src="../images/logo-servus.png" alt="logo Servus"> 



