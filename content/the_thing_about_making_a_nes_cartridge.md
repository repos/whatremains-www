Title: The thing about making a NES/Famicom cartridge
Date: 2016-08-24
Tags: hardware, cartridge

I'm going to simplify things a bit in this rambling,
but hopefully that should help articulate one of the many challenges we're facing with this project.
Challenge in the sense that it is a whole new territory for us :)


So,
assuming you would like to develop a game for a modern platform,
whether it's a console,
a mobile device,
or a desktop/laptop kind of machine,
you are essentially writing software to run within a rather high level environment that has abstracted a lot of the hardware already,
and unless you need to do specific things or optimise parts of the software,
you basically do not have to care too much about the underlying system that will run your code.
As a matter of fact,
you will only be busy shipping binary code in whatever preferred package and format specific to the target platform.
To some extent,
this is what we will be doing as well in this project:
publish a NES/Famicom ROM image and benefit from plethora emulators to run it on myriad operating systems,
and even web browsers.
However we also want to make the project available on physical NES and Famicom cartridges.
This is where things get a bit more complicated.


The thing with machines like the NES/FC is that it differs greatly from,
say,
early home computers,
that often came loaded with an existing ROM containing a small operating system,
and maybe some applications.
This was done so that it could be booted and be used right away even if no software had been loaded externally.
For instance this was the case for the Apple II---also running on a 6502---
that would provide a built-in version of a BASIC interpreter from Microsoft,
as well as some system tools at startup.
Same for other machines,
including the glorious C64 and its 6502 cousin the 6510,
where you could start punching BASIC code and entertain yourself for hours,
days even,
with nothing else but the machine itself[^1]

[^1]: You can still do that nowadays and I highly recommend it.


![](images/Apple_II_BASIC.jpg)


Now,
if you boot a NES/FC without a game cartridge,
you're going to be disapointed because instead ... *nothing* will happen,
or seem to happen at least.
In fact,
when the NES/FC is powered on,
it triggers the RESET interrupt that will point the 6502 CPU to which address it should jump and excecute the first instruction of the software.
Except that the vector address of the RESET interrupt is mapped in the ROM...
and the ROM is in the cartridge...
and the cartridge is... well...
well, it was not inserted in the NES/FC in this example.
Think about it for a second.
It means that the NES/FC is an *incomplete* machine,
only a semi-functionnal computer.
Think about it again,
take a walk,
then come back.


It's actually quite beautiful because it is as if the NES/FC could be completed into any sort of things depending on what part you will connect.
Once this part is connected---this part being the cartridge---
it becomes a computer dedicated to only one specific task.
What is more,
this missing part does not have to be just memory that contains the software.
But first let's have a look at a simple cart board.


![](images/Donkey-Kong_PCB.jpg)

![](images/Kung_Fu_PCB.jpg)


The first carts,
such as the 1983 Donkey Kong Famicom game in the first photo,
and most of the simplest games,
like the 1987 Kung Fu NES game beneath it,
used a board that only contained two Mask ROM chips,
one for the program (PRG),
and one for the pattern tables (CHR) used for the graphics[^2].
In the example above Donkey Kong used 16K PRG and 8K CHR,
and Kung Fu 32K CHR and 8K CHR.
So these chips were quite small,
they could not hold many information,
which was kind of OK because the NES/FC processors could not deal with bigger chips,
and the games of the time,
ported from arcade or inspired by them,
were quite minimal and with little resources.
Still,
the memory mapping limitation of the NES that prevented to access data beyond the 32K (PRG) and 8K (CHR) boundaries,
made it clear soon enough that it also prevented making larger software with more graphics,
a rather important aspect in the growing video game industry at the time.
As a result,
cartridge boards started to be modified to welcome a new chip that would give the ability to the NES/FC to access more memory.
The trick was to make the extra memory available as banks that could be switched to read different regions of the Mask ROMs.
The design of such boards and the chip needed for the operation is often referred to as a mapper.
There are *lots* of mappers,
some developed by Nintendo itself,
some by game companies[^3].
Below is the board from the 1988 game Blaster Master,
the top component is the MMC1B2 mapper chip that permits the splitting into different banks of the *super* large 128K (PRG) and 128K (CHR) Mask ROMs.

[^2]: On the photo above there is a third chip,
but this is in fact specific to the NES and not the FC.
Indeed,
NES carts had an extra chip,
the CIC lockout chip to prevent unlicensed carts to be made and start the infamous regional locking of systems.
Apart from being useless and easily worked around it does not add anything but annoyance,
as any proto-DRM was.

[^3]: The great NES Cart DB lists 79 commercial mappers. See http://bootgod.dyndns.org:7777/pcb.php?search=&treehead=mapper


![](images/Blaster_Master_PCB.jpg)


But like I said,
it did not have to be just memory.
And following the principle that the NES/FC design was giving enough access to the inner part of the incomplete machine,
more sophisticated design allowed the NES/FC to remain on the market,
by not changing the core console,
but by making the incomplete part more and more developped.
Thus,
carts could be as complicated as needed to provide new or extend the existing hardware with dedicated chips,
for instance extra sound chip or coprocessors.
Have a look below to the rather packed cart of the 1987 Family Trainer 3: Aerobics Studio!---I guess you do need all that for an aerobics game.


![](images/aerobics_cart.jpg)


But wait,
there's more!
Following the same principle,
whole extra peripherals could be developped and plugged into the console,
like the amazing Famicom Modem,
that in 1988 gave the machine users the ability to connect to a server so as to download software,
follow stock markets---it's the 80s, checking stock market is the right thing to do after playing aerobics games---and access to a bunch of infotainment services like horse races---also mandatory 80s activity.


![](images/Famicom_Network_System.jpg)


How does all this relate to the project though?
Well,
it's simple,
if we want to make a physical cartridge for the game,
we need to take into consideration its hardware at the same time as we're developing the software and working on the graphics and sound.
Needless to say we have no plan or intention at this point to make a very special board,
but we do need however to decide on a mapper as the 32K/8K limit is simply too much for making anything than simple platform/action/puzzle game mechanics with heavy recycling of sprites and tiles.
It gets a bit more complicated because one thing we will be trying to do is to see how far we can push the idea of recycling existing carts for the project,
which means we might not be able to fully choose the mapper.
So at this point,
because we want to see if we could succeed in publishing the game on a *very* limited run of carts that have been entirely recycled,
we need to find carts we can mod and reuse,
that are both cheap,
easy to find and not collector's item,
and yet with a mapper and possibly other features that can satisfy requirements that are not entirely known yet.


Ahem,
there is a bit of circular thing going on here.
To make our life easier and solve the deadlock,
for now,
we will be likely using an MMC1 based board for the demo and proof-of-concept.
So in the coming weeks,
we'll try recycling something simple without mapper,
and then see if we can start making an MMC1 based prototype for the demo.
More on that later,
as I said in the opening of this post,
this is all new territory for us :)



Image Credits
-------------

* Apple II Plus photos: Todd Harrison
* Famicom cart and PCBs: bootgod, NES Cart Database
* Famicom Modem: レアものブログ ＭＳＸ ファミコン


