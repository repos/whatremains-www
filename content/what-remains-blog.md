Title: Hey, we have a blog!
Date: 2016-05-07 
Tags: news

Hello everyone,
looks like we have a *blog* :)

We will use this space to post updates about the game,
research notes,
and generally document the whole project.
